#  MVP RabbitMQ

## Installation and Running

You should install gem dependencies:

```bash
$ bundle install
```

And after install dependencies you must execute:

```bash
$ cd ./bin
$ chmod +x ./**.sh
```

You can manage you processes with using manage.sh

### For start or stop all workers

**./manager.sh [type] [action]**

```bash
$ ./manager.sh all_workers start
$ ./manager.sh all_workers stop
```

### For normalizers

**./manager.sh [type] [source] [action]**

Examples:

```bash
$ ./manager.sh normalizer source1 start
$ ./manager.sh normalizer source1 restart
$ ./manager.sh normalizer source3 stop
```

### For workers

**./manager.sh [type] [action]**

Examples:

```bash
$ ./manager.sh weirdness start
$ ./manager.sh updater start
$ ./manager.sh updater stop
```
