require_relative '../shared/observer'

class Observer3 < Observer

  def initialize
    super('source3')
  end

  def generate_data
    specific_field = {
        :o3_price   => rand(100..8000),
        :o3_surface => FFaker::NameFR.name,
    }
    super().merge(specific_field)
  end

end
