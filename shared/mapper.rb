require_relative 'wash'

class Mapper < Wash
  property :ads_uid,    from: 'ads_uid'
  property :house_uid,  from: 'house_uid'
  property :date,       from: 'date'
  property :source,     from: 'source'

  # The different fields

  property :price,      from: 'o1_price'
  property :surface,    from: 'o1_surface'
  property :price,      from: 'o2_price'
  property :surface,    from: 'o2_surface'
  property :price,      from: 'o3_price'
  property :surface,    from: 'o3_surface'

end
