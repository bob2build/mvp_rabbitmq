require_relative '../shared/observer'

class Observer2 < Observer

  def initialize
    super('source2')
  end

  def generate_data  
    specific_fields = {
        :o2_price   => rand(100..500),
        :o2_surface => FFaker::NameBR.name,
    }
    super().merge(specific_fields)
  end

end
