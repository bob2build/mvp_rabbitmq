require_relative '../shared/processor.rb'
require 'elasticsearch'


Sneakers.logger.level = Logger::INFO

Sneakers.configure(:daemonize => true,
                   :log => 'logs/updater.log',
                   :threads => 1,
                   :prefetch => 10,
                   :heartbeat => 2,
                   :ack => true,
                   :retry_max_times => 5,
                   :timeout_job_after => 1,
                   :retry_timeout => 1000,
                   :pid_path => 'pids/updater_sources.pid',
                   :handler=> Sneakers::Handlers::Maxretry)

class UpdateProcessor < Processor

  from_queue 'main_ads_queue'

  ## copy the intializer of the worker
  def initialize(a, b)
    super(a, b)
    @client = Elasticsearch::Client.new log: true
  end

  def get_house_doc(data)
    @client.get index: 'advert_index', type: 'ads', id: data["house_uid"]
  end

  def update_house_doc(data)
     client.update index: 'advert_index',
                        type: 'ads',
                        id: data["house_uid"],
                        body: {
                            script: 'ctx._source.ads.add(new_ad)',
                            params: {
                                'new_ad': {
                                    ads_uid: data['ads_uid'],
                                    price: data['price'],
                                    observered_at: data['date']
                                }
                            }
                        }
  end

  def create_house_doc(data)
    @client.index  index: 'advert_index', type: 'ads', body: {
          house_uid: data['house_uid'],
          surface: data['surface'],
          photos: [],
          ads: [
              ads_uid: data['ads_uid'],
              price: data['price'],
              observered_at: data['date']
          ]
      }
  end


  def process(data)
    house = house_exist?(data)
    if house.nil?
      create_house_doc(data)
    else
      update_house_doc(data)
    end
  end

end

