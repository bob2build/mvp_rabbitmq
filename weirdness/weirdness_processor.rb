require_relative '../shared/processor.rb'

class WeirdnessProcessor < Processor

Sneakers.configure(:daemonize => true,
                   :threads => 1,
                   :prefetch => 10,
                   :heartbeat => 2,
                   :retry_max_times => 5,
                   :timeout_job_after => 1,
                   :retry_timeout => 1000,
                   :pid_path => 'pids/weirdness_sources.pid',
                   :log => 'logs/weirdness.log',
                   :handler=> Sneakers::Handlers::Maxretry)

  from_queue 'normalized_ads_queue'

  def process(data)
    if rand(100) <= 5
      #TODO pushing in weirdness queue
      Sneakers::logger.info("Weirdness ads! #{data}")
      Sneakers.publish( data.to_json, :to_queue => 'weird_ads_queue' )
    else
      Sneakers.publish( data.to_json, :to_queue => 'main_ads_queue' )
    end

  end
end

