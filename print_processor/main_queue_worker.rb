require 'sneakers'
require 'sneakers/handlers/maxretry'
require 'json'

class MainQueueWorker
  include Sneakers::Worker


  ##### Configuration ####
  Sneakers.configure(:daemonize => false,
                     :log => STDOUT,
                     :threads => 10,
                     :prefetch => 10,
                     :heartbeat => 2,
                     :retry_max_times => 5,
                     :timeout_job_after => 1,
                     :retry_timeout => 1000)

  Sneakers.logger.level = Logger::INFO

  from_queue :main_queue_ads,
             :handler => Sneakers::Handlers::Maxretry
  ########################

  def work(msg)
    puts "Processed information - #{msg}"
    ack!
  end

end
